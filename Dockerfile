FROM registry.access.redhat.com/ubi9/ubi-minimal

RUN mkdir -p "/opt/alum"

WORKDIR "/opt/alum"
COPY application.toml .
COPY target/release/alum .
COPY target/release/alum-runner .

ENV RUN_MODE=production
ENV ALUM_ALUMRUNNER=/opt/alum/alum-runner

VOLUME [ "/var/alum" ]

ENTRYPOINT ["/opt/alum/alum"]