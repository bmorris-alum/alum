use alum_runner_proto::exec::command_executor_server::{CommandExecutor, CommandExecutorServer};
use alum_runner_proto::exec::{ExecCommandRequest, ExecCommandResult};
use shlex::split;
use std::process;
use std::process::{Command, ExitStatus, Stdio};
use std::time::Duration;
use tokio::task::JoinHandle;
use tonic::{transport::Server, Request, Response, Status};

#[derive(Default)]
struct AlumCommandRunner {}

impl AlumCommandRunner {
    fn run_command(command: ExecCommandRequest) -> JoinHandle<ExecCommandResult> {
        tokio::spawn(async move {
            let lex = split(&command.command_string);
            let command = lex.as_ref().and_then(|c| c.split_first());

            match command {
                None => ExecCommandResult {
                    message: "Failed to parse command line".into(),
                    status_code: 1i32,
                },
                Some((command, args)) => AlumCommandRunner::execute(command, args),
            }
        })
    }

    fn execute(command: &String, args: &[String]) -> ExecCommandResult {
        println!("Executing command {:?} with args {:?}", command, args);
        if command == "exit" {
            println!("Received exit, exit process");
            process::exit(0i32);
        }

        let command = Command::new(command)
            .args(args)
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .status();

        match command {
            Ok(exit_status) => AlumCommandRunner::code_result(&exit_status),
            Err(err) => ExecCommandResult {
                message: err.to_string(),
                status_code: 10,
            },
        }
    }

    fn code_result(code: &ExitStatus) -> ExecCommandResult {
        let exit_code = code.code().unwrap_or(1i32);
        ExecCommandResult {
            message: code.to_string(),
            status_code: exit_code,
        }
    }
}

#[tonic::async_trait]
impl CommandExecutor for AlumCommandRunner {
    async fn execute_command(
        &self,
        request: Request<ExecCommandRequest>,
    ) -> Result<Response<ExecCommandResult>, Status> {
        println!("Received exec command request {:?}", request);
        let result = AlumCommandRunner::run_command(request.into_inner()).await;
        println!("Executed command result {:?}", result);
        let result = result.map_err(|e| Status::from_error(Box::new(e)));
        result.map(Response::new)
    }
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let addr = "0.0.0.0:50051".parse().unwrap();
    let executor = AlumCommandRunner::default();

    println!("GreeterServer listening on {}", addr);

    Server::builder()
        .http2_keepalive_interval(Some(Duration::from_secs(10)))
        .http2_keepalive_timeout(Some(Duration::from_secs(60)))
        .add_service(CommandExecutorServer::new(executor))
        .serve(addr)
        .await?;

    Ok(())
}
