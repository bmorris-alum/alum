use config::{Config, Environment, File};
use serde_derive::Deserialize;
use std::env;
use std::path::PathBuf;
use url::Url;

#[derive(Debug, Clone, Deserialize)]
#[allow(unused)]
pub struct GitlabSettings {
    pub url: Url,
    pub username: String,
    pub apitoken: String,
}

#[derive(Debug, Clone, Deserialize)]
#[allow(unused)]
pub struct DockerSettings {
    pub alumrunner: PathBuf,
    pub nanocpu: Option<i64>,
    pub memorybytes: Option<i64>,
}

#[derive(Debug, Clone, Deserialize)]
#[allow(unused)]
pub struct Metrics {
    pub host: String,
    pub path: String,
    pub port: u16,
}

#[derive(Debug, Clone, Deserialize)]
#[allow(unused)]
pub struct Settings {
    pub workdir: PathBuf,
    pub gitlab: Option<GitlabSettings>,
    pub docker: Option<DockerSettings>,
    pub metrics: Option<Metrics>,
}

impl Settings {
    pub fn new() -> anyhow::Result<Self> {
        let run_mode = env::var("RUN_MODE").unwrap_or_else(|_| "development".into());

        let s = Config::builder()
            .add_source(File::with_name("application"))
            .add_source(File::with_name(&format!("application_{run_mode}")).required(false))
            .add_source(Environment::with_prefix("alum").separator("_"))
            .build()?;
        let settings = s.try_deserialize()?;
        Ok(settings)
    }
}
