#![forbid(unsafe_code)]

use alum_config::AlumConfigurator;
use alum_processor::processor::{AlumProcessor, DefaultProcessor};
use alum_processor::settings::ProcessorSettings;
use axum::routing::get;
use axum::Router;
use configuration::Settings;
use gitlab_graphql::GitlabGraphQl;
use opentelemetry::sdk::trace as sdktrace;
use opentelemetry::sdk::Resource;
use opentelemetry::trace::TraceError;
use opentelemetry_otlp::WithExportConfig;
use opentelemetry_semantic_conventions::resource::{PROCESS_COMMAND, PROCESS_PID, SERVICE_NAME};
use prometheus::{Encoder, TextEncoder};
use std::net::ToSocketAddrs;
use tracing::{error, info, instrument, warn, Level};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

fn init_tracer() -> Result<sdktrace::Tracer, TraceError> {
    let context = Resource::new(vec![
        SERVICE_NAME.string("alum"),
        PROCESS_PID.i64(std::process::id() as i64),
        PROCESS_COMMAND.string(
            std::env::current_exe()
                .map(|e| e.display().to_string())
                .unwrap_or_else(|_| "N/A".to_string()),
        ),
    ]);

    opentelemetry_otlp::new_pipeline()
        .tracing()
        .with_exporter(opentelemetry_otlp::new_exporter().tonic().with_env())
        .with_trace_config(sdktrace::config().with_resource(Resource::default().merge(&context)))
        .install_batch(opentelemetry::runtime::Tokio)
}

fn init_tracing() {
    let tracer = init_tracer().expect("Failed to create OTEL tracer");

    tracing_subscriber::fmt()
        .json()
        .with_span_list(true)
        .with_max_level(Level::INFO)
        .finish()
        .with(tracing_opentelemetry::layer().with_tracer(tracer))
        .init();
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    init_tracing();

    let settings = settings().expect("Failed to read settings");

    info!("{:?}", settings);

    let gitlab_settings = settings.gitlab.as_ref().expect("No gitlab settings");
    let docker_settings = settings.docker.as_ref().expect("No docker settings");
    let configurator = GitlabGraphQl::create(gitlab_settings)?;

    let configs = configurator.alum_configs().await?;

    info!("Configs: {:?}", configs);

    let processor = DefaultProcessor::new(ProcessorSettings {
        work_dir: settings.workdir,
        git_settings: gitlab_settings.into(),
        docker_settings: docker_settings.into(),
    })?;
    for config in configs {
        let process_result = processor.process(&config).await;
        let full_path = config.repo.full_path.to_string();
        match process_result {
            Ok(()) => {
                info!("Processed config {:?}", full_path);
                let mr = configurator
                    .create_merge_request(&config, "alum/tasks", "main")
                    .await?;
                configurator.accept_merge_request(&config, &mr).await?;
            }
            Err(err) => error!("Fail to process config {:?}. {:?}", full_path, err),
        };
    }
    Ok(())
}

#[allow(dead_code)]
async fn start_metrics_server(settings: &Settings) -> anyhow::Result<()> {
    let metrics = settings.metrics.as_ref().unwrap();
    let app = Router::new().route(
        metrics.path.as_str(),
        get(|| async {
            let mut buffer = vec![];
            let encoder = TextEncoder::new();
            let metric_families = prometheus::gather();
            encoder.encode(&metric_families, &mut buffer).unwrap();
            String::from_utf8(buffer).unwrap()
        }),
    );

    let host = format!("{}:{}", metrics.host, metrics.port);
    let addr = host
        .to_socket_addrs()
        .expect("Invalid metrics host:port")
        .next()
        .expect("No address specified");
    info!("listening on {}", addr);
    let srv = axum::Server::bind(&addr).serve(app.into_make_service());

    srv.await?;
    Ok(())
}

#[instrument]
fn settings() -> anyhow::Result<Settings> {
    let settings = Settings::new();
    info!("{:?}", settings);
    settings
}

#[cfg(test)]
mod tests {}
