use graphql_client::GraphQLQuery;
use reqwest::header::{HeaderMap, HeaderValue, ACCEPT, CONTENT_TYPE};
use std::fs;
use std::path::Path;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "src/graphql/introspection_schema.graphql",
    query_path = "src/graphql/introspection_query.graphql",
    response_derives = "Serialize",
    variable_derives = "Deserialize"
)]
#[allow(dead_code)]
struct IntrospectionQuery;

fn main() {
    download_gitlab_schema().expect("Failed to download gitlab graphql schema");
}

fn download_gitlab_schema() -> anyhow::Result<()> {
    println!("cargo:rerun-if-changed=src/graphql/introspection_schema.graphql");
    println!("cargo:rerun-if-changed=src/graphql/introspection_query.graphql");
    let out_file = Path::new("target");
    fs::create_dir_all(out_file)?;
    let out_file = out_file.join("gitlab_schema.json");
    println!(
        "cargo:rustc-env=GITLAB_GRAPHQL_SCHEMA={}",
        out_file.to_str().unwrap()
    );

    let request_body: graphql_client::QueryBody<()> = graphql_client::QueryBody {
        variables: (),
        query: introspection_query::QUERY,
        operation_name: introspection_query::OPERATION_NAME,
    };

    let client = reqwest::blocking::Client::builder().build()?;

    let req_builder = client
        .post("https://gitlab.com/api/graphql")
        .headers(construct_headers());

    let res = req_builder.json(&request_body).send()?;

    let json: serde_json::Value = res.json()?;
    let out = ::std::fs::File::create(out_file)?;

    serde_json::to_writer_pretty(out, &json)?;

    Ok(())
}

fn construct_headers() -> HeaderMap {
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));
    headers.insert(ACCEPT, HeaderValue::from_static("application/json"));
    headers
}
