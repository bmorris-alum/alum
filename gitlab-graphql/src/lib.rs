use crate::alum_configs_query::{
    AlumConfigsQueryProjectsNodes, AlumConfigsQueryProjectsNodesRepository,
    AlumConfigsQueryProjectsNodesRepositoryBlobsNodes,
};
use crate::GitlabError::{InvalidAlumTaskConfig, MultipleAlumTaskConfigs, NoAlumTaskConfig};
use alum_config::{AlumConfig, AlumConfigurator, AlumTasks};
use anyhow::{bail, Context};
use async_trait::async_trait;
use configuration::GitlabSettings;
use graphql_client::GraphQLQuery;
use reqwest_metrics::HttpMetricsMiddleware;
use reqwest_middleware::{ClientBuilder, ClientWithMiddleware};
use reqwest_retry::policies::ExponentialBackoff;
use reqwest_retry::RetryTransientMiddleware;
use reqwest_tracing::TracingMiddleware;
use thiserror::Error;
use tracing::{error, info, instrument};
use url::Url;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "target/gitlab_schema.json",
    query_path = "src/graphql/alum_configs_query.graphql",
    response_derives = "Debug, Serialize",
    variable_derives = "Deserialize"
)]
#[allow(dead_code)]
struct AlumConfigsQuery;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "target/gitlab_schema.json",
    query_path = "src/graphql/create_merge_request_mutation.graphql",
    response_derives = "Debug, Serialize",
    variable_derives = "Deserialize"
)]
#[allow(dead_code)]
struct CreateMergeRequest;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "target/gitlab_schema.json",
    query_path = "src/graphql/accept_merge_request_mutation.graphql",
    response_derives = "Debug, Serialize",
    variable_derives = "Deserialize"
)]
#[allow(dead_code)]
struct AcceptMergeRequest;

#[derive(Debug)]
pub struct GitlabGraphQl {
    pub base_path: Url,
    pub client: ClientWithMiddleware,
}

#[derive(Debug)]
#[allow(dead_code)]
pub struct GitlabMergeRequest {
    id: String,
    diff_head_sha: String,
}

#[derive(Debug)]
#[allow(dead_code)]
pub struct AcceptedGitlabMergeRequest {
    id: String,
    diff_head_sha: String,
}

impl GitlabGraphQl {
    pub fn create(settings: &GitlabSettings) -> anyhow::Result<Self> {
        let client = reqwest::Client::builder()
            .default_headers(
                std::iter::once((
                    reqwest::header::AUTHORIZATION,
                    reqwest::header::HeaderValue::from_str(&format!(
                        "Bearer {}",
                        settings.apitoken
                    ))
                    .unwrap(),
                ))
                .collect(),
            )
            .build()?;
        let retry_policy = ExponentialBackoff::builder().build_with_max_retries(3);
        let client = ClientBuilder::new(client)
            .with(HttpMetricsMiddleware::default())
            .with(TracingMiddleware::default())
            .with(RetryTransientMiddleware::new_with_policy(retry_policy))
            .build();
        let base_path = settings.url.clone();
        Ok(Self { base_path, client })
    }

    fn to_alum_config(&self, node: &AlumConfigsQueryProjectsNodes) -> anyhow::Result<AlumConfig> {
        info!("Processing repo {:?}", node.full_path.to_string());
        let alum_repo = alum_config::Repo {
            id: node.id.to_string(),
            name: node.name.to_string(),
            full_path: node.full_path.to_string(),
            http_url: Url::parse(
                node.http_url_to_repo
                    .as_ref()
                    .context("No repo URL")?
                    .as_str(),
            )?,
        };

        let alum_tasks: anyhow::Result<AlumTasks> = match node.repository.as_ref() {
            None => Err(NoAlumTaskConfig.into()),
            Some(repository) => self.to_alum_tasks(repository),
        };

        info!("Processed alum task config {:?}", alum_tasks);

        alum_tasks.map(|t| AlumConfig {
            repo: alum_repo,
            tasks: t,
        })
    }

    fn to_alum_tasks(
        &self,
        repository: &AlumConfigsQueryProjectsNodesRepository,
    ) -> anyhow::Result<AlumTasks> {
        let nodes: Vec<&AlumConfigsQueryProjectsNodesRepositoryBlobsNodes> = repository
            .blobs
            .as_ref()
            .and_then(|b| b.nodes.as_ref())
            .and_then(|n| n.iter().map(|o| o.as_ref()).collect())
            .unwrap_or(Vec::new());
        match nodes[..] {
            [] => Err(NoAlumTaskConfig.into()),
            [node] => self.blob_to_alum_tasks(node),
            _ => Err(MultipleAlumTaskConfigs.into()),
        }
    }

    fn blob_to_alum_tasks(
        &self,
        repository: &AlumConfigsQueryProjectsNodesRepositoryBlobsNodes,
    ) -> anyhow::Result<AlumTasks> {
        match repository.raw_blob.as_ref() {
            Some(blob) => {
                let t: Result<AlumTasks, serde_yaml::Error> = serde_yaml::from_str(blob.as_str());
                t.map_err(|e| InvalidAlumTaskConfig(e).into())
            }
            None => Err(NoAlumTaskConfig.into()),
        }
    }

    #[instrument(skip_all, fields(config.name = alum_config.repo.name, merge.source=source_branch, merge.target=target_branch))]
    pub async fn create_merge_request(
        &self,
        alum_config: &AlumConfig,
        source_branch: &str,
        target_branch: &str,
    ) -> anyhow::Result<GitlabMergeRequest> {
        let variables = create_merge_request::Variables {
            project_path: alum_config.repo.full_path.to_string(),
            title: "Alum tasks".to_string(),
            source_branch: source_branch.to_string(),
            target_branch: target_branch.to_string(),
            description: Some("Created by Alum based on .alum-config.yaml".to_string()),
        };
        let request_body = CreateMergeRequest::build_query(variables);
        let response_body = self
            .client
            .post(self.base_path.as_str())
            .json(&request_body)
            .send()
            .await?;

        let response_body: graphql_client::Response<create_merge_request::ResponseData> =
            response_body.json().await?;

        info!("Got merge request create response {:?}", response_body);

        let response_data: create_merge_request::ResponseData =
            response_body.data.context("missing response data")?;

        let response_data = response_data
            .merge_request_create
            .context("missing response merge_request_create")?;

        if !response_data.errors.is_empty() {
            bail!("Failed to create merge request {:?}", response_data.errors)
        }

        let response_data = response_data
            .merge_request
            .context("missing merge request data")?;

        let sha = response_data.diff_head_sha.context("Did not receive SHA")?;
        Ok(GitlabMergeRequest {
            id: response_data.iid,
            diff_head_sha: sha,
        })
    }

    #[instrument(skip_all, fields(config.name = alum_config.repo.name, mr.iid = merge_request.id))]
    pub async fn accept_merge_request(
        &self,
        alum_config: &AlumConfig,
        merge_request: &GitlabMergeRequest,
    ) -> anyhow::Result<AcceptedGitlabMergeRequest> {
        let variables = accept_merge_request::Variables {
            project_path: alum_config.repo.full_path.to_string(),
            iid: merge_request.id.clone(),
            sha: merge_request.diff_head_sha.clone(),
        };
        let request_body = AcceptMergeRequest::build_query(variables);
        let response_body = self
            .client
            .post(self.base_path.as_str())
            .json(&request_body)
            .send()
            .await?;

        let response_body: graphql_client::Response<accept_merge_request::ResponseData> =
            response_body.json().await?;

        info!("Got merge request accept response {:?}", response_body);

        let response_data: accept_merge_request::ResponseData =
            response_body.data.context("missing response data")?;

        let response_data = response_data
            .merge_request_accept
            .context("missing response merge_request_create")?;

        if !response_data.errors.is_empty() {
            bail!("Failed to accept merge request {:?}", response_data.errors)
        }

        let response_data = response_data
            .merge_request
            .context("missing merge request data")?;

        let sha = response_data.diff_head_sha.context("Did not receive SHA")?;
        Ok(AcceptedGitlabMergeRequest {
            id: response_data.iid,
            diff_head_sha: sha,
        })
    }
}

#[async_trait]
impl AlumConfigurator for GitlabGraphQl {
    #[instrument]
    async fn alum_configs(&self) -> anyhow::Result<Vec<AlumConfig>> {
        let variables = alum_configs_query::Variables { after: None };

        let request_body = AlumConfigsQuery::build_query(variables);
        let response_body = self
            .client
            .post(self.base_path.as_str())
            .json(&request_body)
            .send()
            .await?;
        let response_body: graphql_client::Response<alum_configs_query::ResponseData> =
            response_body.json().await?;

        let response_data: alum_configs_query::ResponseData =
            response_body.data.context("missing response data")?;

        let nodes: Vec<AlumConfigsQueryProjectsNodes> = response_data
            .projects
            .and_then(|p| p.nodes)
            .and_then(|n| n.into_iter().collect())
            .unwrap_or(Vec::new());

        let configs: Vec<AlumConfig> = nodes
            .into_iter()
            .flat_map(|node| {
                let config = self.to_alum_config(&node);
                if config.is_err() {
                    info!(
                        "Failed to parse Alum config for {:?} {:?}",
                        node.full_path, config
                    )
                }
                config.into_iter()
            })
            .collect();

        info!("Gathered Alum configs {:?}", configs);

        Ok(configs)
    }
}

#[derive(Error, Debug)]
pub enum GitlabError {
    #[error("No alum task configuration in repo")]
    NoAlumTaskConfig,
    #[error("Multiple alum task configurations in repo")]
    MultipleAlumTaskConfigs,
    #[error("Failed to parse alum task config")]
    InvalidAlumTaskConfig(#[from] serde_yaml::Error),
}

#[cfg(test)]
mod tests {
    use crate::alum_configs_query::AlumConfigsQueryProjectsNodesRepositoryBlobsNodes;
    use crate::GitlabGraphQl;
    use configuration::GitlabSettings;
    use std::fs;
    use url::Url;

    #[tokio::test]
    async fn test_parse_invalid_config_yaml() {
        let gitlab_settings = GitlabSettings {
            url: Url::parse("http://localhost/").expect("URL"),
            username: "".to_string(),
            apitoken: "".to_string(),
        };
        let gitlab_graphql = GitlabGraphQl::create(&gitlab_settings).expect("Create cut");

        let raw_blob =
            fs::read_to_string("tests/alum_config/invalid-config.yaml").expect("Test file");
        let nodes = AlumConfigsQueryProjectsNodesRepositoryBlobsNodes {
            raw_blob: Some(raw_blob),
        };

        let parse_result = gitlab_graphql.blob_to_alum_tasks(&nodes);

        assert!(parse_result.is_err())
    }
}
