use crate::processor::git::{
    AlumGitHandler, AlumGitRepository, Git2AlumGitHandler, Git2GitRepository,
};
use crate::processor::tasks::{AlumTaskHandler, BollardAlumTaskHandler};
use crate::settings::ProcessorSettings;
use alum_config::AlumConfig;
use anyhow::Context;
use async_trait::async_trait;
use std::path::Path;
use tracing::{info, instrument};

mod git;
mod tasks;

#[async_trait]
pub trait AlumProcessor {
    async fn process(&self, config: &AlumConfig) -> anyhow::Result<()>;
}

pub struct DefaultProcessor<G, T>
where
    G: AlumGitHandler,
    T: AlumTaskHandler,
{
    settings: ProcessorSettings,
    git_handler: G,
    task_handler: T,
}

impl DefaultProcessor<Git2AlumGitHandler, BollardAlumTaskHandler> {
    pub fn new(settings: ProcessorSettings) -> anyhow::Result<Self> {
        let processor = Self {
            settings: settings.clone(),
            git_handler: Git2AlumGitHandler::new(settings.git_settings),
            task_handler: BollardAlumTaskHandler::new(settings.docker_settings)?,
        };
        Ok(processor)
    }

    async fn run_tasks(&self, work_dir: &Path, config: &AlumConfig) -> anyhow::Result<()> {
        let tasks = config.tasks.tasks.clone();
        for task in tasks {
            self.task_handler
                .execute_task(work_dir, config, task)
                .await?;
        }
        Ok(())
    }

    fn add_and_push(&self, repo: &Git2GitRepository) -> anyhow::Result<()> {
        repo.add()?;
        let has_changes = repo.has_changes()?;
        if has_changes {
            repo.commit("Alum tasks".to_string())?;
            repo.push()?;
        }
        Ok(())
    }
}

#[async_trait]
impl AlumProcessor for DefaultProcessor<Git2AlumGitHandler, BollardAlumTaskHandler> {
    #[instrument(skip_all, fields(config.name = config.repo.name))]
    async fn process(&self, config: &AlumConfig) -> anyhow::Result<()> {
        let repo = self
            .git_handler
            .create_repository(&self.settings.work_dir, config)
            .await?;
        let work_dir = repo.work_dir().context("No working directory from clone")?;
        info!("Cloned into work dir {:?}", work_dir);
        self.run_tasks(work_dir, config).await?;
        self.add_and_push(&repo)?;
        Ok(())
    }
}
