use crate::processor::tasks::DockerError::{NonZeroTaskExitStatus, UnknownContainerExitStatus};
use crate::settings::DockerSettings;
use alum_config::{AlumConfig, AlumTask};
use alum_runner_proto::exec::command_executor_client::CommandExecutorClient;
use alum_runner_proto::exec::{ExecCommandRequest, ExecCommandResult};
use anyhow::Context;
use async_trait::async_trait;
use bollard::container::{
    Config, CreateContainerOptions, LogsOptions, StopContainerOptions, UploadToContainerOptions,
    WaitContainerOptions,
};
use bollard::image::CreateImageOptions;
use bollard::models::MountTypeEnum::BIND;
use bollard::models::RestartPolicyNameEnum::NO;
use bollard::models::{ContainerWaitResponse, Mount, PortBinding};
use bollard::secret::HostConfig;
use bollard::service::{PortMap, RestartPolicy};
use bollard::Docker;
use flate2::write::GzEncoder;
use flate2::Compression;
use futures::{StreamExt, TryStreamExt};
use reqwest::Url;
use std::collections::HashMap;
use std::fmt::{Debug, Display, Formatter};
use std::fs;
use std::fs::File;
use std::path::Path;
use std::sync::Arc;
use thiserror::Error;
use tokio::task::JoinHandle;
use tracing::{info, instrument, warn};
use users::{get_effective_gid, get_effective_uid};

#[async_trait]
pub trait AlumTaskHandler {
    async fn execute_task(
        &self,
        work_dir: &Path,
        config: &AlumConfig,
        task_config: AlumTask,
    ) -> anyhow::Result<()>;
}

pub struct BollardAlumTaskHandler {
    docker: Arc<Docker>,
    docker_settings: DockerSettings,
}

static CONTAINER_WORKING_DIR: &str = "/workspace";

impl BollardAlumTaskHandler {
    pub fn new(docker_settings: DockerSettings) -> anyhow::Result<Self> {
        let docker = Docker::connect_with_local_defaults()?;
        Ok(Self {
            docker: Arc::new(docker),
            docker_settings,
        })
    }

    async fn create_image(&self, task_config: &AlumTask) -> anyhow::Result<()> {
        self.docker
            .create_image(
                Some(CreateImageOptions {
                    from_image: task_config.image.to_string(),
                    ..CreateImageOptions::default()
                }),
                None,
                None,
            )
            .try_collect::<Vec<_>>()
            .await?;

        Ok(())
    }

    async fn create_container(
        &self,
        work_dir: &Path,
        task_config: &AlumTask,
    ) -> anyhow::Result<BollardContainer> {
        let workdir_mount = Mount {
            source: Some(
                fs::canonicalize(work_dir)?
                    .to_str()
                    .context("Workdir to string")?
                    .to_string(),
            ),
            target: Some(CONTAINER_WORKING_DIR.to_string()),
            typ: Some(BIND),
            read_only: Some(false),
            consistency: Some("cached".to_string()),
            ..Mount::default()
        };

        let mut port_map: PortMap = HashMap::new();
        port_map.insert(
            "50051/tcp".to_string(),
            Some(vec![PortBinding {
                host_ip: Some("127.0.0.1".to_string()),
                host_port: Some("50051".to_string()),
            }]),
        );

        let host_config = HostConfig {
            nano_cpus: self.docker_settings.nano_cpu,
            memory: self.docker_settings.memory_bytes,
            restart_policy: Some(RestartPolicy {
                name: Some(NO),
                maximum_retry_count: None,
            }),
            auto_remove: Some(true),
            cap_drop: Some(vec!["all".to_string()]),
            privileged: Some(false),
            mounts: Some(vec![workdir_mount]),
            port_bindings: Some(port_map),
            ..HostConfig::default()
        };
        let env: Vec<String> = task_config
            .env
            .iter()
            .map(|(k, v)| format!("{k}={v}"))
            .collect();

        let uid = get_effective_uid();
        let gid = get_effective_gid();
        let user = format!("{uid}:{gid}");

        let empty = HashMap::<(), ()>::new();
        let mut exposed_ports = HashMap::new();
        let exposed_port = "50051/tcp".to_string();
        exposed_ports.insert(exposed_port.as_str(), empty);

        let container_config: Config<&str> = Config {
            env: Some(env.iter().map(AsRef::as_ref).collect()),
            user: Some(user.as_str()),
            entrypoint: Some(vec!["/alum-runner"]),
            image: Some(task_config.image.as_str()),
            working_dir: Some(CONTAINER_WORKING_DIR),
            host_config: Some(host_config),
            exposed_ports: Some(exposed_ports),
            ..Config::default()
        };

        let container = BollardContainer::create_container(
            self.docker_settings.clone(),
            self.docker.clone(),
            container_config,
        )
        .await?;
        Ok(container)
    }
}

struct BollardContainer {
    docker_settings: DockerSettings,
    docker: Arc<Docker>,
    container_id: String,
}

impl BollardContainer {
    async fn create_container(
        docker_settings: DockerSettings,
        docker: Arc<Docker>,
        container_config: Config<&str>,
    ) -> anyhow::Result<Self> {
        let create_options = CreateContainerOptions {
            name: "alum-runner",
            ..CreateContainerOptions::default()
        };
        let container_id = docker
            .create_container::<&str, &str>(Some(create_options), container_config)
            .await?
            .id;
        Ok(BollardContainer {
            docker_settings,
            docker: docker.clone(),
            container_id,
        })
    }

    async fn start(&self) -> Result<(), bollard::errors::Error> {
        self.docker
            .start_container::<String>(&self.container_id, None)
            .await
    }

    async fn upload_alum_runner(&self) -> anyhow::Result<()> {
        let mut contents = Vec::new();
        {
            let enc = GzEncoder::new(&mut contents, Compression::default());
            let mut tar = tar::Builder::new(enc);
            let mut f = File::open(&self.docker_settings.alum_runner)?;
            tar.append_file("alum-runner", &mut f)?;
        }

        let upload_options = UploadToContainerOptions {
            path: "/",
            ..UploadToContainerOptions::default()
        };
        self.docker
            .upload_to_container(&self.container_id, Some(upload_options), contents.into())
            .await?;

        Ok(())
    }

    #[allow(dead_code)]
    fn container_status(&self) -> JoinHandle<anyhow::Result<ContainerWaitResponse>> {
        let mut container_status = self
            .docker
            .wait_container(&self.container_id, None::<WaitContainerOptions<String>>);

        let status: JoinHandle<anyhow::Result<ContainerWaitResponse>> = tokio::spawn(async move {
            let mut response: anyhow::Result<ContainerWaitResponse> =
                Err(UnknownContainerExitStatus.into());
            while let Some(wait_response) = container_status.next().await {
                info!("Wait response {:?}", wait_response);
                response = wait_response.map_err(anyhow::Error::from);
            }
            response
        });
        status
    }

    fn print_logs(&self) -> anyhow::Result<JoinHandle<()>> {
        let mut stream = self.docker.logs::<String>(
            &self.container_id,
            Some(LogsOptions {
                follow: true,
                stdout: true,
                stderr: true,
                ..Default::default()
            }),
        );

        let handle = tokio::spawn(async move {
            while let Some(Ok(msg)) = stream.next().await {
                print!("{}", msg);
            }
        });

        Ok(handle)
    }
}

impl Drop for BollardContainer {
    fn drop(&mut self) {
        let id = self.container_id.clone();
        let docker = self.docker.clone();
        tokio::spawn(async move {
            let remove = docker
                .stop_container(&id, Some(StopContainerOptions { t: 5i64 }))
                .await;
            if remove.is_err() {
                warn!("Failed to stop container {:?}. {:?}", id, remove)
            } else {
                info!("Stopped container {:?}.", id)
            }
        });
    }
}

impl Display for BollardContainer {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Container {:?}", self.container_id)
    }
}

impl Debug for BollardContainer {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Container {:?}", self.container_id)
    }
}

struct AlumRunnerClient {
    runner_client: CommandExecutorClient<tonic::transport::Channel>,
}

impl AlumRunnerClient {
    async fn connect(url: Url) -> anyhow::Result<Self> {
        let url = url.to_string();
        let runner_client = CommandExecutorClient::connect(url).await?;
        Ok(Self { runner_client })
    }

    async fn execute_command(
        &mut self,
        r: ExecCommandRequest,
    ) -> anyhow::Result<ExecCommandResult> {
        let request = tonic::Request::new(r);
        info!("Sending request to Alum Runner {:?}", request);
        let response = self.runner_client.execute_command(request).await?;
        info!("Received response from Alum Runner {:?}", response);

        let response = response.into_inner();
        Ok(response)
    }

    async fn execute_and_check(&mut self, command: &str) -> anyhow::Result<()> {
        let response = self
            .execute_command(ExecCommandRequest {
                command_string: command.to_string(),
            })
            .await?;

        if response.status_code != 0i32 {
            return Err(NonZeroTaskExitStatus(response.status_code, response.message).into());
        }
        Ok(())
    }

    #[instrument(skip_all, fields(task.name = task_config.name))]
    async fn execute_tasks(&mut self, task_config: &AlumTask) -> anyhow::Result<()> {
        info!("Running before");
        for command in &task_config.before {
            self.execute_and_check(command).await?;
        }
        info!("Running script");
        for command in &task_config.script {
            self.execute_and_check(command).await?;
        }
        info!("Running after");
        for command in &task_config.after {
            self.execute_and_check(command).await?;
        }
        Ok(())
    }
}

impl Drop for AlumRunnerClient {
    fn drop(&mut self) {
        let mut client = self.runner_client.clone();
        tokio::spawn(async move {
            let request = tonic::Request::new(ExecCommandRequest {
                command_string: "exit".into(),
            });
            info!("Sending request to Alum Runner {:?}", request);
            let response = client.execute_command(request).await;
            let response = response.map(tonic::Response::into_inner);
            info!("Received response from Alum Runner {:?}", response);
            if response.is_err() {
                warn!("Failed to send exit to alum runner. {:?}", response)
            } else {
                info!("Sent exit to Alum Runner");
            }
        });
    }
}

#[async_trait]
impl AlumTaskHandler for BollardAlumTaskHandler {
    #[instrument(skip_all, fields(config.name = _config.repo.name, task.name = task_config.name))]
    async fn execute_task(
        &self,
        work_dir: &Path,
        _config: &AlumConfig,
        task_config: AlumTask,
    ) -> anyhow::Result<()> {
        self.create_image(&task_config).await?;

        let container = self.create_container(work_dir, &task_config).await?;
        info!("Container {:?} created", container);
        container.upload_alum_runner().await?;
        container.start().await?;
        info!("Container started");
        let _logs = container.print_logs()?;
        info!("Streaming logs for container");
        let client_url = Url::parse("http://localhost:50051")?;
        let mut runner_client = AlumRunnerClient::connect(client_url).await?;

        runner_client.execute_tasks(&task_config).await?;
        Ok(())
    }
}

#[derive(Error, Debug)]
#[allow(dead_code)]
pub enum DockerError {
    #[error("Could not determine container exit status")]
    UnknownContainerExitStatus,
    #[error("Non-zero task exist status {0} {1}")]
    NonZeroTaskExitStatus(i32, String),
}
