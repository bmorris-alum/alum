use crate::processor::git::GitError::ExistingAlumBranch;
use crate::settings::GitSettings;
use alum_config::AlumConfig;
use anyhow::Context;
use async_trait::async_trait;
use git2::BranchType::Remote;
use git2::{Direction, IndexAddOption, Repository, Signature};
use regex::Regex;
use std::fs::remove_dir_all;
use std::path::Path;
use thiserror::Error;
use tracing::{info, instrument};

#[async_trait]
pub trait AlumGitHandler {
    type Repoistory;

    async fn create_repository(
        &self,
        work_dir: &Path,
        config: &AlumConfig,
    ) -> anyhow::Result<Self::Repoistory>;
}

#[async_trait]
pub trait AlumGitRepository {
    fn work_dir(&self) -> Option<&Path>;
    fn current_branch(&self) -> anyhow::Result<String>;
    fn remote_default_branch(&self) -> anyhow::Result<String>;
    fn add(&self) -> anyhow::Result<()>;
    fn has_changes(&self) -> anyhow::Result<bool>;
    fn commit(&self, message: String) -> anyhow::Result<()>;
    fn push(&self) -> anyhow::Result<()>;
}

pub struct Git2AlumGitHandler {
    settings: GitSettings,
}

impl Git2AlumGitHandler {
    pub fn new(settings: GitSettings) -> Self {
        Self { settings }
    }
}

pub struct Git2GitRepository {
    repository: Repository,
}

impl Git2GitRepository {
    pub fn new(repository: Repository) -> Self {
        Self { repository }
    }
}

static BRANCH_NAME: &str = "alum/tasks";

impl AlumGitRepository for Git2GitRepository {
    fn work_dir(&self) -> Option<&Path> {
        self.repository.workdir()
    }

    fn current_branch(&self) -> anyhow::Result<String> {
        let repo = &self.repository;
        let head = repo.head()?;
        head.shorthand()
            .map(std::string::ToString::to_string)
            .context("No current branch")
    }

    fn remote_default_branch(&self) -> anyhow::Result<String> {
        let mut remote = self.repository.find_remote("origin")?;
        remote.connect(Direction::Fetch)?;
        let default_branch = remote.default_branch()?;

        default_branch
            .as_str()
            .map(std::string::ToString::to_string)
            .context("No default remote branch for origin")
    }

    #[instrument(skip_all)]
    fn add(&self) -> anyhow::Result<()> {
        let repo = &self.repository;
        let mut index = repo.index()?;

        index.add_all(["*"].iter(), IndexAddOption::CHECK_PATHSPEC, None)?;
        index.write()?;
        Ok(())
    }

    #[instrument(skip_all)]
    fn has_changes(&self) -> anyhow::Result<bool> {
        let repo = &self.repository;
        let head = repo.head()?;
        let commit = head.peel_to_commit()?;
        let tree = commit.tree()?;
        let diff = repo.diff_tree_to_index(Some(&tree), None, None)?;
        let stats = diff.stats()?;
        info!("Diff to HEAD {:?}", stats);
        let changes = stats.files_changed() + stats.deletions() + stats.insertions();
        Ok(changes > 0)
    }

    #[instrument(skip_all)]
    fn commit(&self, message: String) -> anyhow::Result<()> {
        let repo = &self.repository;
        let signature = Signature::now("Alum", "alum@borismorris.co.uk")?;
        let mut index = repo.index()?;
        let tree_oid = index.write_tree()?;
        let tree = repo.find_tree(tree_oid)?;
        let head = repo.head()?;
        let commit = head.peel_to_commit()?;
        repo.commit(
            Some("HEAD"),
            &signature,
            &signature,
            message.as_str(),
            &tree,
            &[&commit],
        )?;
        Ok(())
    }

    fn push(&self) -> anyhow::Result<()> {
        let mut remote = self.repository.find_remote("origin")?;
        remote.connect(Direction::Push)?;
        let refspec = format!("refs/heads/{BRANCH_NAME}:refs/heads/{BRANCH_NAME}");
        remote.push(&[refspec], None)?;
        Ok(())
    }
}

impl Drop for Git2GitRepository {
    fn drop(&mut self) {
        if let Some(work_dir) = self.work_dir() {
            remove_dir_all(work_dir).expect("Failed to cleanup");
        }
    }
}

#[async_trait]
impl AlumGitHandler for Git2AlumGitHandler {
    type Repoistory = Git2GitRepository;

    #[instrument(skip_all, fields(config.name = config.repo.name))]
    async fn create_repository(
        &self,
        work_dir: &Path,
        config: &AlumConfig,
    ) -> anyhow::Result<Git2GitRepository> {
        let options = sanitize_filename::Options {
            truncate: true,
            windows: true,
            replacement: "_",
        };
        let repo_name_safe =
            sanitize_filename::sanitize_with_options(&config.repo.full_path, options);
        let repo_path = work_dir.join(repo_name_safe);
        let mut credential_url = config.repo.http_url.clone();
        credential_url
            .set_username(self.settings.username.as_str())
            .ok()
            .context("Failed to set username")?;
        credential_url
            .set_password(Some(self.settings.password.as_str()))
            .ok()
            .context("Failed to set password")?;
        let repo = Repository::clone(credential_url.as_str(), repo_path)?;

        let branches = repo.branches(Some(Remote))?;

        let re = Regex::new(r"^.*?/alum/tasks$").unwrap();
        let alum_branches: Vec<String> = branches
            .flat_map(|branch| branch.ok())
            .inspect(|(branch, branch_type)| info!("Branch: {:?} {:?}", branch.name(), branch_type))
            .flat_map(|(branch, _)| match branch.name() {
                Ok(Some(branch_name)) => Some(branch_name.to_string()),
                Ok(None) => None,
                Err(_) => None,
            })
            .filter(|branch_name| re.is_match(branch_name))
            .collect();
        if !alum_branches.is_empty() {
            let alum_branches = alum_branches.join(":");
            return Err(ExistingAlumBranch(alum_branches).into());
        }
        {
            let head = repo.head()?;
            let commit = head.peel_to_commit()?;
            repo.branch(BRANCH_NAME, &commit, false)?;
        }
        {
            let (object, reference) = repo.revparse_ext(BRANCH_NAME)?;
            repo.checkout_tree(&object, None)?;
            if let Some(gref) = reference {
                repo.set_head(gref.name().unwrap())?;
            }
        }

        Ok(Git2GitRepository::new(repo))
    }
}

#[derive(Error, Debug)]
pub enum GitError {
    #[error("Existing Alum branch in repo {0}")]
    ExistingAlumBranch(String),
}
