use configuration::GitlabSettings;
use std::path::PathBuf;

#[derive(Clone)]
pub struct GitSettings {
    pub username: String,
    pub password: String,
}

impl GitSettings {
    pub fn new(username: String, password: String) -> Self {
        Self { username, password }
    }
}

#[derive(Clone)]
pub struct DockerSettings {
    pub alum_runner: PathBuf,
    pub nano_cpu: Option<i64>,
    pub memory_bytes: Option<i64>,
}

#[derive(Clone)]
pub struct ProcessorSettings {
    pub work_dir: PathBuf,
    pub git_settings: GitSettings,
    pub docker_settings: DockerSettings,
}

impl From<&GitlabSettings> for GitSettings {
    fn from(value: &GitlabSettings) -> Self {
        GitSettings::new(value.username.to_string(), value.apitoken.to_string())
    }
}

impl From<&configuration::DockerSettings> for DockerSettings {
    fn from(value: &configuration::DockerSettings) -> Self {
        Self {
            alum_runner: value.alumrunner.clone(),
            nano_cpu: value.nanocpu,
            memory_bytes: value.memorybytes,
        }
    }
}
