use async_trait::async_trait;
use serde_derive::Deserialize;
use std::collections::BTreeMap;
use url::Url;

#[derive(Debug, Clone, Deserialize)]
pub struct AlumTask {
    pub name: String,
    pub image: String,
    #[serde(default)]
    pub env: BTreeMap<String, String>,
    #[serde(default)]
    pub before: Vec<String>,
    pub script: Vec<String>,
    #[serde(default)]
    pub after: Vec<String>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct AlumTasks {
    pub tasks: Vec<AlumTask>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Repo {
    pub id: String,
    pub name: String,
    pub full_path: String,
    pub http_url: Url,
}

#[derive(Debug, Clone, Deserialize)]
pub struct AlumConfig {
    pub repo: Repo,
    pub tasks: AlumTasks,
}

#[async_trait]
pub trait AlumConfigurator {
    async fn alum_configs(&self) -> anyhow::Result<Vec<AlumConfig>>;
}
