use async_trait::async_trait;
use lazy_static::lazy_static;
use prometheus::{register_histogram_vec, HistogramVec};
use reqwest::header::USER_AGENT;
use reqwest::{Request, Response};
use reqwest_middleware::{Middleware, Next};
use std::time::{Duration, SystemTime};
use task_local_extensions::Extensions;

lazy_static! {
    static ref HTTP_CLIENT_REQUESTS: HistogramVec = register_histogram_vec!(
        "http_client_requests",
        "help",
        &["method", "uri", "status", "outcome", "clientName"]
    )
    .unwrap();
}

#[derive(Default)]
pub struct HttpMetricsMiddleware {}

impl HttpMetricsMiddleware {
    pub fn new() -> Self {
        Self {}
    }

    fn client_name(req: &Request) -> String {
        let headers = req.headers();
        headers
            .get(USER_AGENT)
            .and_then(|h| h.to_str().ok())
            .map(|h| h.to_string())
            .unwrap_or_else(|| "Unknown".to_string())
    }

    fn method(req: &Request) -> String {
        req.method().to_string()
    }

    fn path(req: &Request) -> String {
        req.url().path().to_string()
    }
}

#[async_trait]
impl Middleware for HttpMetricsMiddleware {
    async fn handle(
        &self,
        req: Request,
        extensions: &mut Extensions,
        next: Next<'_>,
    ) -> reqwest_middleware::Result<Response> {
        let start = SystemTime::now();
        let client_name = HttpMetricsMiddleware::client_name(&req);
        let method = HttpMetricsMiddleware::method(&req);
        let path = HttpMetricsMiddleware::path(&req);
        let response = next.run(req, extensions).await;

        let elapsed = SystemTime::now()
            .duration_since(start)
            .ok()
            .map(duration_to_seconds);
        let outcome = if response.is_ok() {
            "SUCCESS"
        } else {
            "FAILED"
        };
        let status = if let Ok(r) = response.as_ref() {
            r.status().to_string()
        } else {
            "0".to_string()
        };
        let metric = HTTP_CLIENT_REQUESTS.with_label_values(&[
            method.as_str(),
            path.as_str(),
            status.as_str(),
            outcome,
            client_name.as_str(),
        ]);
        if let Some(duration) = elapsed {
            metric.observe(duration);
        }
        response
    }
}

#[inline]
fn duration_to_seconds(d: Duration) -> f64 {
    let nanos = f64::from(d.subsec_nanos()) / 1e9;
    d.as_secs() as f64 + nanos
}
